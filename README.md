# order in chaos

Trying to desperately put some order in modular arithmetics. The core idea is to separate the domain[0,n] into subdomains [f(i),f(i+1)], each of which is projected into [i*n,(i+1)*n] if the fonction f is an increasing function over each subdomain. Doing so, you should be able to find a single "a" such that f(a)=x in that interval.  
Doing that for fun. That's fun and restorative.


# square root
Sqrt.c is an attemps at finding square roots of x modulo n.
First the Square root program search for it in the interval [0,sqrt(n)]. If it doesn't find it, it searches in [sqrt(n),sqrt(2n)], then in  [sqrt(2n),sqrt(3n)] etc... targets are merely parsed linearly between p and p²/4, there might be better ideas.
square root seems to have a good behavior for small numbers, I noticed some inconsistencies on bigger numbers. Complexity is hard to evaluate, I divide the domain into n/4 subdomains,stopping at n/2, each of which being much smaller than n.They are parsed with a dichotomia algorithm, so finding a specific square root is often fast, but not finding it can lead to long waits. If it grows like n, it grows by a very small constant.
sqrt2.c does the same, but don't bother with dividing into intervals, it takes only [0,n]. Less fun but faster.
sqrt runs an additionnal loop over all the values in order to find all square roots of a given number.
Next step is even less aimed at success and much more a free fall into madness,  searching for a link between a real value for the square root and an integer solution over a ring.



## Collaborate with your team

I merely don't have a team, no offense. 

## Test and Deploy


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
