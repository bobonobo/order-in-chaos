#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>

int64_t size(int64_t t){
	if(t==0){
		return 0;
	}
	int64_t ct=0;
	while(t!=0){
		t=(t>>1);
		ct++;
	}
	return ct;
}


int64_t squart(int64_t p,int64_t inf, int64_t sup ){
	int64_t tmp;
	int64_t a = inf;
	int64_t b = sup;
	int64_t m = ((a+b)>>1);
	while( b!=a||b!=a+1 ){
		tmp=m*m;
		
		if((tmp<=p-m-0.25) && (tmp>=p-(m<<1)-1)){
			return m+1;
		}
		if((tmp>p-m-0.25) && (tmp<=p)){
			return m;
		}
		else if(tmp>p){
			b=m;
		}
		else if(tmp<p-(m<<1)-1){
			a=m;
		}
		m=(a+b)>>1;
	}
	return -1;
}
int64_t squartMod(int64_t n,int64_t inf, int64_t sup ){
	int64_t tmp;
	int64_t a = inf;
	int64_t b = sup;
	int64_t m = ((a+b)>>1);
	while( (b!=(a+1))&&(b!=a) ){
		//sleep(1);
		tmp=m*m;
		//printf("a: %ld \n",a);
		//printf("b: %ld \n",b);
		//printf("tmp: %ld \n",tmp);
		
		if(tmp==n){
			return m;
		}
		else if(tmp>n){
			b=m;
		}
		else if(tmp<n){
			a=m;
		}
		
		m=(a+b)>>1;
		
	}
	//printf("a: %ld \n",a);
	//printf("b: %ld \n",b);
	//printf("tmp: %ld \n",tmp);
		
	return -1;
}
int main(int argc, char *argv[]){

	int64_t i;
	int64_t r;
	/*
	for(i=0;i<400;i++){
		r=squart(i,0,i);
		printf("sqrt(%ld):%ld\n",i,r);
	}
	*/
	int64_t p= 65537 ;//39916801;//479001599;65537 ;
	//int64_t n= 6720402;
	printf("size(%ld):%ld\n",p,size(p));
	int64_t n= 0;

	int64_t k=0;
	for(int64_t j = n; j< p;j++){
		int64_t a=0;
		int64_t b=0;
		for(i=0;i<(p>>2);i++){
			a=b;
			b=squart((i+1)*p,0,p);
			r= squartMod(j+i*p,a,b);
			//printf("r: %ld \n",r);
			//printf("i: %ld \n",i);
			//printf("p/4: %ld \n",p>>2);
			if(r!=-1){
				printf("sqrt(%ld) mod %ld :%ld\n",j,p,r);
				printf("sqrt(%ld) mod %ld :%ld\n",j,p,p-r);
				printf("p/2: %ld \n",p>>1);
				printf("i: %ld \n",i);
				k++;	
				break;
			}		
		}
	}
	printf("nombre de racines trouvées: %ld \n",2*k);
	/*
	for(i=0;i<(p>>2);i++){
		a=b;
		b=squart((i+1)*p,0,p);
		r= squartMod(n+i*p,a,b);
		printf("r: %ld \n",r);
		printf("i: %ld \n",i);
		printf("p/4: %ld \n",p>>2);
		if(r!=-1){
			printf("sqrt(%ld) mod %ld :%ld\n",n,p,r);
			printf("p/2: %ld \n",p>>1);
			printf("i: %ld \n",i);	
			break;
		}		
	}
	*/
return 0;


}